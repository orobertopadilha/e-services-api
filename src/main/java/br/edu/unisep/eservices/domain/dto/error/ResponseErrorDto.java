package br.edu.unisep.eservices.domain.dto.error;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ResponseErrorDto {

    private final String message;

}
