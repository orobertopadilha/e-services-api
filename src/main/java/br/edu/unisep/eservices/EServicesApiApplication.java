package br.edu.unisep.eservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "br.edu.unisep")
public class EServicesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EServicesApiApplication.class, args);
	}

}
